PyNeander v. 1.0
-----------------------------------------------------------

1.Interface
   1.1 Barra de Op��es
   1.2 Tabelas
   1.3 ACC e PC
   1.4 JN e JZ
   1.5 Output

2.Como Usar
   2.1 Abrindo arquivo
   2.2 Debugando o c�digo
   2.3 Compilando o c�digo
3.Sobre

-----------------------------------------------------------

1.Interface
   1.1 Barra de Op��es
   	A barra de op��es se localiza acima das tabelas,
possui bot�es para Compilar, Debugar um c�digo , passar 
para Pr�xima Instru��o e Abrir um arquivo.

   1.2 Tabelas
       A tabela Instructions mostra as instru��es contidas
no arquivo *.mem carregado.
       A tabela de valores mostra os valores contidos no
arquivo.

   1.3 ACC e PC
       O campo ACC mostra o valor armazenado em ACC no 
momento.
       O campo PC mostra a pr�xima instru��o a ser buscada.

   1.4 JN e JZ
       Os campos JN e JZ mudam de cor quando a verifica��o
de menor que zero ou zero respectivamente retornam positivo.

   1.5 Output
       O campo de Output mostra todas as instru��es 
executadas. As instru��es s�o mostradas em mnem�nico. 

2. Como Usar
   2.1 Abrindo arquivo
	Para abrir um arquivo, aperte o menu File e no 
bot�o Open File, utilize o atalho na barra de op��es ou 
utilize o atalho CTRL + O.
	O programa � compat�vel apenas com arquivos *.mem

   2.2 Debugando o c�digo
	No menu File utilize a op��o Debug ou utilize o 
bot�o Debug na barra de op��es. O atalho para op��o � 
CTRL + D.
	Ap�s apertar a op��o Debug utiliza o bot�o Next
Instruction para passar para a pr�xima instru��o, ou o atalho
CTRL+N.
        A op��o Compile estar� desabilitada no modo Debug

   2.3 Compilando o c�digo
	Utilize o bot�o Compile na barra de op��es ou a 
op��o Compile no menu File. O atalho para a op��o � CTRL + C
 
3. Sobre
PyNeander � um compilador de Neander programado em Python 3,
com interface gr�fica utilizando PySide.
 