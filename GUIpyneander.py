# -*- encoding: utf-8 -*-

import sys
import pyneander2 as pyn2

from PySide import QtGui, QtCore
from PySide.QtCore import Qt
from PySide.QtGui import *

class MainForm(QMainWindow):
    def __init__(self):
        super(MainForm, self).__init__()
        self.initUI()
        self.__filename = str()
        self.__fileloaded = False
        self.__modedebug = False
        self.__item = QTableWidgetItem()
        self.__item.setBackground(QColor(225,15,15))
        self.__item.setFont(QFont('Arial Black', 9))
        self.__item.setForeground(QColor(255,255,255))

    def initUI(self):
        #=====================================================#
        #                       Tables                        #
        #=====================================================#
        # Table instruictions
        self.tableInstructions = QTableWidget(128,1,self)
        self.tableInstructions.setStyleSheet('''
                                        QHeaderView::section{
                                        spacing: 10px;
                                        background-color:rgb(69,69,69);
                                        color: white;
                                        margin: 1px;
                                        text-align: right;
                                        font-family: ubuntu;
                                        font-size:12px;}''')
        self.tableInstructions.resize(200, 250)
        self.tableInstructions.move(5, 60)
        self.tableInstructions.setHorizontalHeaderLabels(['Instructions'])
        self.tableInstructions.setVerticalHeaderLabels(['#' + str(i)
                                                   for i in range(128)])

        # Table values
        self.tableValues = QTableWidget(128, 1, self)
        self.tableValues.setStyleSheet('''
                                        QHeaderView::section{
                                        spacing: 10px;
                                        background-color:rgb(69,69,69);
                                        color: white;
                                        margin: 1px;
                                        text-align: right;
                                        font-family: ubuntu;
                                        font-size:12px;}''')
        self.tableValues.resize(200, 250)
        self.tableValues.move(5, 320)
        self.tableValues.setHorizontalHeaderLabels(['Values'])
        self.tableValues.setVerticalHeaderLabels(['#' + str(i)
                                             for i in range(128, 257)])
        #=======================Tables========================#


        #=====================================================#
        #                      Actions                        #
        #=====================================================#
        # compile
        compileAction = QAction(QIcon('compile.png'), '&Compile', self)
        compileAction.setShortcut('Ctrl+C')
        compileAction.setStatusTip('Compile the code')
        compileAction.triggered.connect(self.compile)

        # debug
        debugAction = QAction(QIcon('debug.png'), '&Debug', self)
        debugAction.setShortcut('Ctrl+D')
        debugAction.setStatusTip('Debug the code')
        debugAction.triggered.connect(self.debug)

        # next intruction
        nextInstruction = QAction(QIcon('debug_next.png'), '&Next', self)
        nextInstruction.setShortcut('Ctrl+N')
        nextInstruction.setStatusTip('Next line of file')
        nextInstruction.triggered.connect(self.nextinstruction)

        #open file
        openfileAction = QAction(QIcon('openfile.png'), '&Open File', self)
        openfileAction.setShortcut('Ctrl+O')
        openfileAction.setStatusTip('Open file')
        openfileAction.triggered.connect(self.openfiledialog)
        #=====================Actions=======================#


        #===================================================#
        #                     Labels                        #
        #===================================================#
        # Acc
        labelACC = QLabel('ACC', self)
        labelACC.setStyleSheet('font-size:11px; font-family: Arial Black')
        labelACC.move(300,60)

        # Pc
        labelPC = QLabel('PC', self)
        labelPC.setStyleSheet('font-size:11px; font-family: Arial Black')
        labelPC.move(300, 110)

        # JN
        labelTextJN = QLabel('JN', self)
        labelTextJN.setStyleSheet('''
                    border: 1px solid blue;
                    border-radius: 3px;
                    background-color: white;
                    font-family: Arial Black;
                    qproperty-alignment: AlignCenter;
                    ''');
        labelTextJN.move(300, 200)
        self.labelJN = QLabel(self)
        self.labelJN.setPixmap(QPixmap('traffic_light_off.png'))
        self.labelJN.move(400, 200)

        # JZ
        labelTextJZ = QLabel('JZ', self)
        labelTextJZ.setStyleSheet('''
                    border: 1px solid red;
                    border-radius: 3px;
                    background-color: white;
                    font-family: Arial Black;
                    qproperty-alignment: AlignCenter;
                    ''');
        labelTextJZ.move(300, 235)
        self.labelJZ = QLabel(self)
        self.labelJZ.setPixmap(QPixmap('traffic_light_off.png'))
        self.labelJZ.move(400, 235)

        #=====================Labels========================#


        #===================================================#
        #                   LCDNumbers                      #
        #===================================================#
        # Acc
        self.accDisplayNumber = QtGui.QLCDNumber(self)
        self.accDisplayNumber.setStyleSheet('''background-color:rgb(69,69,69);
                                    color:green;
                                    border:1px solid rgb(69,69,69)''')
        self.accDisplayNumber.move(300, 80)

        # PC
        self.pcDisplayNumber = QLCDNumber(self)
        self.pcDisplayNumber.setStyleSheet('''background-color:rgb(69,69,69);
                        color:green;border:1px solid rgb(69,69,69)''')
        self.pcDisplayNumber.move(300,130)
        #===================LCDNumbers======================#


        #===================================================#
        #                   TextEdit                        #
        #===================================================#
        self.text = QTextEdit(self)
        self.text.setReadOnly(True)
        self.text.setStyleSheet('''
                            border-radius:3px;
                            font-size: 14px;
                            font-width: bold;
                            font-family: Courier, Droid;
                            border: 2px solid white;
                            background-color:rgb(169,169,169);''')
        self.text.resize(265,250)
        self.text.move(210,320)
        #===================LCDNumbers======================#


        #===================================================#
        #                    Menubar                        #
        #===================================================#
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(compileAction)
        fileMenu.addSeparator()
        fileMenu.addAction(debugAction)
        fileMenu.addAction(nextInstruction)
        fileMenu.addSeparator()
        fileMenu.addAction(openfileAction)
        #=====================Menubar=======================#


        #===================================================#
        #                    ToolBar                        #
        #===================================================#
        toolbar = self.addToolBar(' ')
        toolbar.setStyleSheet('''
                                background: rgba(69, 69, 69);
                                padding: 1px;
                                border-radius: 3px;
                                margin: 1px
                             ''')
        toolbar.addAction(compileAction)
        toolbar.addSeparator()
        toolbar.addAction(debugAction)
        toolbar.addAction(nextInstruction)
        toolbar.addSeparator()
        toolbar.addAction(openfileAction)
        #====================ToolBar========================#

        self.setGeometry(100,50, 480, 620)
        self.setWindowTitle('PyNeander')

        self.statusBar()
        self.statusBar().setStyleSheet('''
                                background: rgba(69, 69, 69);
                                padding: 5px;
                                border-radius: 1px;
                                color: white;
                                ''')
        self.setWindowIcon(QIcon('icon.ico'))
        self.show()

    def nextinstruction(self):
        if self.__modedebug:
            if not pyn2.halt:
                self.accDisplayNumber.display(pyn2.acc)
                self.pcDisplayNumber.display(pyn2.pc)

                if pyn2.jz:
                    self.labelJZ.setPixmap(QPixmap('traffic_light.png'))
                else:
                    self.labelJZ.setPixmap(QPixmap('traffic_light_off.png'))

                if pyn2.jn:
                    self.labelJN.setPixmap(QPixmap('traffic_light.png'))
                else:
                    self.labelJN.setPixmap(QPixmap('traffic_light_off.png'))

                code = self.mnemonic(pyn2.memory[pyn2.pc])
                
                self.text.append('<font color="red" face="Arial Black">' + code + '</font>')

                pyn2.address, pyn2.code, pyn2.acc, pyn2.pc, pyn2.jn, pyn2.jz, pyn2.halt = pyn2.decode(pyn2.memory, \
                pyn2.mapmemory, pyn2.address, pyn2.code, pyn2.acc, pyn2.pc, pyn2.jn, pyn2.jz, pyn2.halt)

                address = pyn2.pc

                if code is 'JMP':
                    self.text.append('<font face="Arial Black">' + str(address+1) + '</font>')
                elif code is 'JZ' and pyn2.jz:
                    self.text.append('<font face="Arial Black">' + str(address) + '</font>')
                elif code is 'JN' and pyn2.jn:
                    self.text.append('<font face="Arial Black">' + str(address) + '</font>')
                elif code is not 'JN' and code is not 'JZ' and code is not 'HLT' and address >= 0:
                    self.text.append('<font face="Arial Black">' + str(pyn2.memory[pyn2.pc-1]) + '</font>')
                    
                self.ldaTablesM()
                
            else:
                QMessageBox.information(self, 'Operation successful', 'Code debugged successfully!!!')
        else:
            QMessageBox.warning(self, 'Warning', 'Select debug mode before performing this operation.')

    def mnemonic(self, number):
        if number == 1: return 'STA'
        elif number == 2: return 'LDA'
        elif number == 3: return 'ADD'
        elif number == 4: return 'OR'
        elif number == 5: return 'AND'
        elif number == 6: return 'NOT'
        elif number == 8: return 'JMP'
        elif number == 9: return 'JN'
        elif number == 10: return 'JZ'
        elif number == 15: return 'HLT'

    def debug(self):
        if self.__fileloaded:
            self.clearMainWindow()
            self.__modedebug = True
            self.ldaTables(self.__filename)
            QMessageBox.information(self, 'Altered vision', 'The mode was changed to debug interpreter!!!')
        else:
            QMessageBox.critical(self, 'Error', 'Load the source file before debugging the code!!!')

    def compile(self):
        if self.__fileloaded and not self.__modedebug:
            pyn2.makearchive()
            self.ldaTables('saida.mem')
            self.accDisplayNumber.display(pyn2.acc)
            self.pcDisplayNumber.display(pyn2.pc)

            if pyn2.jz:
                self.labelJZ.setPixmap(QPixmap('traffic_light.png'))
            else:
                self.labelJZ.setPixmap(QPixmap('traffic_light_off.png'))

            if pyn2.jn:
                self.labelJN.setPixmap(QPixmap('traffic_light.png'))
            else:
                self.labelJN.setPixmap(QPixmap('traffic_light_off.png'))

            self.text.setText('Operation termined with sucess!!!')
        elif self.__modedebug:
            QMessageBox.critical(self, 'Error', 'Mode debug is actived')
        else:
            QMessageBox.critical(self, 'Error', 'Load the source file before compiling the code!!!')

    def ldaTables(self, path):
        with open(path) as f:
            for line in f.readlines():
                line = line.split(' ')
                address = int(line[0])
                item = QTableWidgetItem()
                item.setBackground(QColor(199,203,200))
                item.setFont(QFont('Arial Black', 9))
                item.setText(line[1])
                if  address >= 0 and address <= 127:
                    self.tableInstructions.setItem(address - 1, 1, item)
                else:
                    self.tableValues.setItem(address - 129, 1, item)

    def ldaTablesM(self):
        for pos in range(256):
            i = QTableWidgetItem()
            i.setBackground(QColor(199,203,200))
            i.setFont(QFont('Arial Black', 9))
            i.setText(str(pyn2.memory[pos]))
            if pyn2.memory[pos] is not None:
                if pos < 128:    
                    self.tableInstructions.setItem(pos-1, 1, i)
                else:
                    self.tableValues.setItem(pos-129, 1, i)

    def clearMainWindow(self):
        self.text.clear()
        self.text.clear()
        for i in range(-1, 127): self.tableInstructions.setItem(i, 1, QTableWidgetItem())
        for i in range(-1, 127): self.tableValues.setItem(i, 1, QTableWidgetItem())
        self.accDisplayNumber.display(0)
        self.pcDisplayNumber.display(0)
        self.labelJZ.setPixmap(QPixmap('traffic_light_off.png'))
        self.labelJN.setPixmap(QPixmap('traffic_light_off.png'))
        self.__modedebug = False

    def openfiledialog(self):
        dialog = QFileDialog(self)
        self.__filename = dialog.getOpenFileName(self, 'Select file', '/', 'source code (*.mem)')[0]
        pyn2.mapmemory = pyn2.ldamap(self.__filename)
        pyn2.ldamemory(pyn2.mapmemory, pyn2.memory)
        QMessageBox.information(self, 'Operation successful', 'The file was opened successfully')
        self.__fileloaded = True
        self.clearMainWindow()


app = QApplication(sys.argv)
mainform = MainForm()
sys.exit(app.exec_())
