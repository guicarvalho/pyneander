def decode(memory, mapmemory, address, code, acc, pc, jn, jz, halt):
	address, code = pc, memory[pc]
	if code == 1: #STA
		pc, address, code = nextline(pc, mapmemory)
		sta(memory, code, acc)
		pc += 1
	elif code == 2: #LDA
		pc, address, code = nextline(pc, mapmemory)
		acc = lda(memory, code)
		pc += 1
	elif code == 3: #ADD
		pc, address, code = nextline(pc, mapmemory)
		acc = add(memory, code, acc)
		pc += 1
	elif code == 4: #OR
		pc, acc = nextline(pc, mapmemory)
		_or(memory, pc, acc)
		pc += 1
	elif code == 5: #AND
		pc, acc = nextline(pc, mapmemory)
		_and(memory, pc, acc)
		pc += 1
	elif code == 6: #NOT
		acc = _not(acc) 
		pc += 1
	elif code == 8: #JMP
		pc = jmp(memory, pc)
	elif code == 9: #JN
		if acc < 0:
			pc = jmp(memory, pc)
			jn = True
		else:
			pc += 2
			jn = False
	elif code == 10: #JZ
		if acc == 0:
			pc = jmp(memory, pc)
			jz = True
		else:
			pc += 2
			jz = False
	elif code == 15: #HTL
		halt = True
	return address, code, acc, pc, jn, jz, halt
	
def jmp(memory,pc):		
	return memory[pc + 1]
	
def sta(memory, code, acc):
	memory[code] = acc
	
def lda(memory, code):
	acc = normal2complement(memory[code])
	return acc

def add(memory, code, acc):
	acc += normal2complement(memory[code])
	return acc
	
def _or(memory, pc, acc):
	acc = acc | normal2complement(memory[pc])

def _and(memory, pc, acc):
	acc = acc & normal2complement(memory[pc])
	
def _not(acc):
	acc = ~acc
	return acc

def nextline(pc, mapmemory):
	pc += 1
	line = mapmemory[pc].split(' ')
	return (pc, int(line[0]), int(line[1]))

def ldamemory(mapmemory, memory):
	for line in mapmemory:
		line = line.split(' ')
		memory[int(line[0])] = int(line[1])
		
def ldamap(path):
	f = open(path)
	return f.readlines()

def normal2complement(number):
	return int.from_bytes([number], byteorder='big', signed=True)

def makearchive():
	global memory, mapmemory, address, code, acc, pc, jn, jz, halt
	while not halt:
		address, code, acc, pc, jn, jz, halt = \
		decode(memory, mapmemory, address, code, acc, pc, jn, jz, halt)
	f = open('saida.mem', 'w')
	i = 0
	for line in memory:
		if line is not None:
			f.write(str(i) + ' ' +str(line) + '\n')
		i += 1
	f.close()

def reset():
        jn = jz = halt = False
        acc = pc = address = code = int()

memory = [None]*256
mapmemory = list()
jn = jz = halt = False
acc = pc = address = code = int()
